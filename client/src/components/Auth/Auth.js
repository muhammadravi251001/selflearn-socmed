import React, { useState } from 'react';
import { Avatar, Button, Paper, Grid, Typography, Container } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import useStyles from './styles'
import Input from './Input';
import { GoogleOAuthProvider, GoogleLogin } from '@react-oauth/google';
import Icon from './icon';
import { useDispatch } from 'react-redux';
import jwt_decode from 'jwt-decode';

const Auth = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [isSignup, setIsSignup] = useState(false);

    const [showPassword, setShowPassword] = useState(false)

    const handleShowPassword = () => {
        setShowPassword((prevShowPassword) => !prevShowPassword)
    };

    const handleSubmit = () => {

    };

    const handleChange = () => {

    };

    const switchMode = () => {
        setIsSignup((prevIsSignup) => !prevIsSignup);
        handleShowPassword(false);
    };

    const googleSuccess = async (res) => {
        //const decoded = jwt_decode(res.credential);
        const result = res?.credential;
        //const token = res?.tokenId;

        try {
            dispatch({ type: 'AUTH', data: { result } });
        } catch (error) {
            console.log(error)
        }

        //console.log(decoded);
    };

    const googleFailure = (error) => {
        console.log(error);
        console.log("Google sign in was unsuccessful. Try again later");
    };

    return (

        <GoogleOAuthProvider clientId='19262151886-9tkffv5euqrehjk5n0ksqfo3e2ed6sm6.apps.googleusercontent.com'>
            <Container component="main" maxWidth="xs">
                <Paper className={classes.paper} elevation={3}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography variant="h5">
                        {isSignup ? 'Sign-up' : 'Sign-in'}
                    </Typography>
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            {
                                isSignup && (
                                    <>
                                        <Input 
                                        name="firstName" 
                                        label="First Name" 
                                        handleChange={handleChange} 
                                        autofocus 
                                        isHalf 
                                        />

                                        <Input 
                                        name="lastName" 
                                        label="Last Name" 
                                        handleChange={handleChange} 
                                        isHalf 
                                        />
                                    </>
                                )
                            }
                            <Input 
                            name="email" 
                            label="Email Address"
                            handleChange={handleChange}
                            type="email"
                            />

                            <Input 
                            name="password" 
                            label="Password"
                            handleChange={handleChange}
                            type={showPassword ? "text" : "password"}
                            handleShowPassword={handleShowPassword}
                            />

                            {isSignup && 
                                <>
                                    <Input 
                                    name="confirmPassword"
                                    label="Repeat Password"
                                    handleChange={handleChange}
                                    type="password"
                                    />
                                </>
                            }
                        </Grid>

                        <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}> 
                            {isSignup ? 'Sign-Up' : 'Sign-In'}
                        </Button>

                        <GoogleLogin 
                            render={(renderProps) => (
                                <Button 
                                className={classes.googleButton} 
                                color="primary" 
                                fullWidth 
                                onClick={renderProps.onClick} 
                                disabled={renderProps.disabled}
                                startIcon={<Icon />}
                                variant="contained"
                                >
                                    Google Sign-In
                                </Button>
                            )}
                            onSuccess={googleSuccess}
                            onFailure={googleFailure}
                            cookiePolicy="single_host_origin"
                        />

                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Button onClick={switchMode}>
                                    {isSignup ? 
                                    'Already have an account? Sign-In!' : 
                                    'Do not have an account? Sign-Up!'
                                    }
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>
            </Container>
        </GoogleOAuthProvider>

    );
}

export default Auth